package ComparableComparator;

public class Company implements Taxable{
	private String nameCompany;
	private double earning, expenses;
	
	public Company(String nameCompany,double earning,double expenses){
		this.nameCompany = nameCompany;
		this.earning = earning;
		this.expenses = expenses;
		
	}
	
	public String getNameCompany(){
		return nameCompany;
	}
	
	public double getEarning(){
		return earning;
	}
	
	public double getExpenses(){
		return expenses;
	}
	
	public double getProfit(){
		return earning-expenses;
	}
	
	public String toString() {
		return "Company[name = "+this.nameCompany+", earning = "+this.earning+", expenses = "+this.expenses+", tex = "+getTax()+"]";
	}
	
	@Override
	public double getTax() {
		return ((0.3)*getProfit());
	}
}
