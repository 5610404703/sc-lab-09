package ComparableComparator;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company c1, Company c2) {
		double companies1 = (c1.getEarning());
		double companies2 = (c2.getEarning());
		if (companies1 > companies2) return 1;
		if (companies1 < companies2) return -1;
		return 0;
	}

}
