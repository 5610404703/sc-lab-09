package ComparableComparator;

public class Person implements  Taxable, Comparable<Person>{
	private String name;
	private double height, emolument;
	
	public Person(String name, double emolument){
		this.name = name;
		this.emolument = emolument;
	}
	
	public String getName(){
		return name;
	}
	
	public double getHeight(){
		return height;
	}
	
	public double getEmolument(){
		return emolument;
	}

	public String toString() {
		return "Person[name = "+this.name+", emolument = "+this.emolument+", tex = "+getTax()+"]";
	}
	
	@Override
	public double getTax() {
		if(this.emolument>=0 && this.emolument<= 300000){			
			return (0.05)*this.emolument;
		}
		return (0.05)*300000 + (0.1)*(this.emolument-300000);
	}
	
	@Override
	public int compareTo(Person other) {
		if (this.emolument < other.emolument ) { return -1; }
		if (this.emolument > other.emolument ) { return 1;  }
		return 0;
	}

	
}