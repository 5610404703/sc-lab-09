package ComparableComparator;

public class Product implements Taxable, Comparable<Product> {
	private String nameProduct;
	private double price;
	public Product(String nameProduct,double price){
		this.nameProduct = nameProduct;
		this.price = price;
		
	}
	
	public String getNameProduct(){
		return nameProduct;
	}
	
	public double price(){
		return price;
	}
	
	public String toString() {
		return "Product[name product = "+this.nameProduct+", price = "+this.price+", tex = "+getTax()+"]";
	}
	
	@Override
	public double getTax() { 
		return (0.07)*price;
	}
	
	@Override
	public int compareTo(Product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
}
