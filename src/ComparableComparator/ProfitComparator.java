package ComparableComparator;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company> {

	@Override
	public int compare(Company c1, Company c2) {
		double profit1 = (c1.getEarning());
		double profit2 = (c2.getEarning());
		if (profit1 > profit2) return 1;
		if (profit1 < profit2) return -1;
		return 0;
	}

}
