package ComparableComparator;

public interface Taxable {
	public double getTax();
}
