package ComparableComparator;

import java.util.ArrayList;
import java.util.Collections;

public class Test {

	public static void main(String[] args) {
		// ------------------- Person ---------------------
			ArrayList<Person> persons = new ArrayList<Person>();
			persons.add(new Person("A", 100000));				
			persons.add(new Person("B", 500000));
			persons.add(new Person("C", 300000));

			System.out.println("---before sort emolument in person");
			for (Person p : persons) 
				System.out.println(p);

			Collections.sort(persons);
			
			System.out.println("\n---after sort emolument in person");
			for (Person p : persons) 
				System.out.println(p);
			
			System.out.println("\t\t---------------------------\n");
				
		// ------------------- Product ---------------------
			ArrayList<Product> products = new ArrayList<Product>();
			products.add(new Product("A", 580));				
			products.add(new Product("B", 1000));
			products.add(new Product("C", 120));
	
			System.out.println("---before sort price in product");
			for (Product pro : products) 
				System.out.println(pro);
	
			Collections.sort(products);
			
			System.out.println("\n---after sort price in product");
			for (Product pro : products) 
				System.out.println(pro);
			
			System.out.println("\t\t---------------------------\n");
			
		// ------------------- Product ---------------------
			ArrayList<Company> companies = new ArrayList<Company>();
			companies.add(new Company("A", 1000000, 800000));				
			companies.add(new Company("B", 1500000, 500000));
			companies.add(new Company("C", 2000000, 1200000));
	
			System.out.println("---before sort company");
			for (Company c : companies) 
				System.out.println(c);
	
			Collections.sort(companies, new EarningComparator());
			
			System.out.println("\n---after sort with earning");
			for (Company c : companies) 
				System.out.println(c);

			Collections.sort(companies, new ExpenseComparator());
			
			System.out.println("\n---after sort with expense");
			for (Company c : companies) 
				System.out.println(c);
			
			Collections.sort(companies, new ProfitComparator());
			
			System.out.println("\n---after sort with profit");
			for (Company c : companies) 
				System.out.println(c);
			
			System.out.println("\t\t---------------------------\n");

		// ------------------- Tax ---------------------
			ArrayList<Taxable> tax = new ArrayList<Taxable>();
			Person per = new Person("A", 100000);
			Product pro = new Product("B", 1000);
			Company com = new Company("C", 2000000, 1200000);
			tax.add(per);				
			tax.add(pro);
			tax.add(com);
	
			System.out.println("---before sort tax");
			for (Taxable t : tax) 
				System.out.println(t);
	
			Collections.sort(tax, new TexComparator());
			
			System.out.println("\n---after sort tax");
			for (Taxable t : tax) 
				System.out.println(t);
			
			System.out.println("\t\t---------------------------\n");
	}

}
