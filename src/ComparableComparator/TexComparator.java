package ComparableComparator;

import java.util.Comparator;

public class TexComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable t1, Taxable t2) {
		double o1 = (t1.getTax());
		double o2 = (t2.getTax());
		if (o1 > o2) return 1;
		if (o1 < o2) return -1;
		return 0;
	} 
	
}
