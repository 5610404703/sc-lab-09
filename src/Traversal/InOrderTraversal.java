package Traversal;

import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> list = new ArrayList<Node>();
		
		
		if( node.getLeft() != null ){
			List<Node> listNode = traverse(node.getLeft());
			for(int i = 0;i < listNode .size();i++)
				list.add(listNode.get(i));
		}
		
		list.add(node);
		
		if (node.getRight() != null){
			List<Node> listNode = traverse(node.getRight());
			for(int i = 0;i < listNode.size();i++)
				list.add(listNode.get(i));
		}
		
		return list;
	}

}
