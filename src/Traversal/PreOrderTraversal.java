package Traversal;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		
		ArrayList<Node> list = new ArrayList<Node>();
		
		list.add(node);
		if (node.getLeft() != null){
			ArrayList<Node> listNode = traverse(node.getLeft());
			for(int i = 0;i < listNode .size();i++)
				list.add(listNode.get(i));
		}
		if(node.getRight() != null){
			ArrayList<Node> listNode = traverse(node.getRight());
			for(int i = 0;i < listNode.size();i++)
				list.add(listNode.get(i));
		}
		
		return list;
	}

}
