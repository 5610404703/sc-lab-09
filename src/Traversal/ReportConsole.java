package Traversal;

public class ReportConsole{
	Traversal traversal;
	Node root;
	public void display(Node root,Traversal traversal){
		this.traversal = traversal;
		this.root = root;
		System.out.println(traversal.traverse(root));
	}

}
