package Traversal;

public class TraverseTest {

	public static void main(String[] args) {
		ReportConsole console = new ReportConsole();
		Node c = new Node("C", null, null);
		Node e = new Node("E", null, null);
		Node d = new Node("D", c, e);
		Node a = new Node("A", null, null);
		Node b = new Node("B", a, d);
		Node h = new Node("H", null, null);
		Node i = new Node("I", h, null);
		Node g = new Node("G", null, i);
		Node f = new Node("F", b, g);
				
		System.out.print("Traverse with PreOrderTraversal : ");
		console.display(f, new PreOrderTraversal());
		
		System.out.print("Traverse with InOrderTraversal : ");
		console.display(f, new InOrderTraversal());
		
		System.out.print("Traverse with PostOrderTraversal : ");
		console.display(f, new PostOrderTraversal());
	}

}
